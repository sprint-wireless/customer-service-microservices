package com.synechron.customerservicemicroservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerServiceMicroservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceMicroservicesApplication.class, args);
	}

}
